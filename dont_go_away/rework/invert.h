#ifndef DEF_INV
#define DEF_INV
Uint32 obtenirPixel(SDL_Surface *surface, int x, int y);
void definirPixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
void miroir(SDL_Surface *surface);
#endif // DEF_INV
