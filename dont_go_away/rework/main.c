/*
Projet :
Choses � ajouter/changer :
    - Syst�me de plusieurs maps
    - faire un "jeu.c" pour d�charger le main
    - Dans le main, faire un menu pour choisir le niveau, etc...
    - jeu prendra en argument le nom de la map � jouer, et retournera un int -> 0 => Quitter, autre => continuer
    - on pourra faire une map choisie ou le mode aventure
*/

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include "main.h"
#include "invert.h"
#include "ia.h"
#include "map.h"
#include "affichage.h"
#include "deplacement.h"
#include "jeu.h"

int main(int argc, char* argv[])
{
    //D�clarations
    int continuer = 1; //Boucle de jeu
    int i; //pour les for (malloc)

    int **carte = NULL; // la carte � charger

    Position posF; //la position de la fille
    Position posG; //la position du gar�on

    Position posFinit;
    Position posGinit;

    SDL_Surface *ecran;

    SDL_Event event;

    //Initialisations
    carte = malloc(sizeof(*carte)*LARGEUR_MAP);
    for(i = 0 ; i < LARGEUR_MAP ; i++)
    {
        carte[i] = malloc(sizeof(**carte)*HAUTEUR_MAP);
    }
    fillMap(carte, "titre.map", &posF, &posG); //initialise la map et les positions

    posF.orientation = HAUT;
    posF.booleen = 0;

    posG.orientation = HAUT;
    posG.booleen = 0;

    posFinit = posF;
    posGinit = posG;

    SDL_Init(SDL_INIT_VIDEO); //Initalisation SDL

    SDL_WM_SetIcon(SDL_LoadBMP("sprites/icone.bmp"), NULL); //Icone -> image concours
    SDL_WM_SetCaption("Don't go away", NULL); // Titre

    ecran = SDL_SetVideoMode(LARGEUR_ECRAN, HAUTEUR_ECRAN, 32, SDL_HWSURFACE|SDL_DOUBLEBUF); //mise en place de l'�cran

    SDL_EnableKeyRepeat(50,50); //R�p�tition des touches

    while(continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
                break;
            case SDL_KEYUP:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        continuer = 0;
                        break;
                    case SDLK_r:
                        posF = posFinit;
                        posG = posGinit;
                        break;
                    default:
                        break;
                }
                break;
            case SDL_KEYDOWN: // adapter � un menu, avec un appui sur une touche pour lancer le jeu
                switch(event.key.keysym.sym)
                {
                    case SDLK_UP:
                        posF.orientation = HAUT;
                        posF = deplacement(posF, carte);
                        posG = deplG(carte, posG, posF);
                        posF.booleen = 1-posF.booleen;
                        break;
                    case SDLK_LEFT:
                        posF.orientation = GAUCHE;
                        posF = deplacement(posF, carte);
                        posG = deplG(carte, posG, posF);
                        posF.booleen = 1-posF.booleen;
                        break;
                    case SDLK_RIGHT:
                        posF.orientation = DROITE;
                        posF = deplacement(posF, carte);
                        posG = deplG(carte, posG, posF);
                        posF.booleen = 1-posF.booleen;
                        break;
                    case SDLK_DOWN:
                        posF.orientation = BAS;
                        posF = deplacement(posF, carte);
                        posG = deplG(carte, posG, posF);
                        posF.booleen = 1-posF.booleen;
                        break;
                    case SDLK_RETURN:
                        switch(carte[posF.x][posF.y])
                        {
                            case AVENTURE:
                                posF = posFinit;
                                posG = posGinit;
                                continuer = jeuAventure(ecran);
                                break;
                            case JLIBRE:
                                posF = posFinit;
                                posG = posGinit;
                                continuer = jeuLibre(ecran);
                                break;
                            default:
                                break;
                        }
                    default:
                        break;
                }
            default:
                break;
        }
        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
        afficheMap(carte, ecran);
        affichePers(posF, posG, ecran);
        switch(carte[posG.x][posG.y])
        {
            case AVENTURE:
                posF = posFinit;
                posG = posGinit;
                continuer = jeuAventure(ecran);
                break;
            case JLIBRE:
                posF = posFinit;
                posG = posGinit;
                break;
            default:
                break;
        }
        SDL_Flip(ecran);
    }

    SDL_Quit();

    delMap(carte);

    return 0;
}
