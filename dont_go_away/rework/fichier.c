#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

#include "fichier.h"

int compteMap()
{
    char carac = 'a';

    int nbLignes = 0;

    FILE *fichier = NULL;

    fichier = fopen("listeMap.txt", "r");

    while(carac != EOF)
    {
        carac = fgetc(fichier);
        if(carac == '\n')
            nbLignes ++;
    }

    fclose(fichier);
    return nbLignes;
}

void setNomMap(char *nomMap, int i)
{
    char nom[50] = {'\0'};

    int ligneAct = 0;

    FILE *fichier = NULL;

    fichier = fopen("listeMap.txt", "r");

    do
    {
        fgets(nom, 50, fichier);
        ligneAct ++;
    }while(ligneAct <= i);

    strcpy(nomMap,"cartes/");
    strcat(nomMap, nom);

    for(i = 0 ; i < strlen(nomMap) ; i++)
    {
        if(nomMap[i] == '\n' || nomMap[i] == '\r')
            nomMap[i] = '\0';
    }

    fclose(fichier);
}

int listeMap(char ***listeNom, DIR *directory, char* dirName) //met les noms des différentes maps dans listeNom et retourne le nombre de maps
{
    int i;
    int nbMap = 0;
    Dirent* fichierLu = NULL;
    //Compte les maps
    while((fichierLu = readdir(directory)) != NULL)
    {
        if ((strcmp(fichierLu->d_name, ".") != 0) && (strcmp(fichierLu->d_name, "..") != 0))
        {
            nbMap ++;
        }
    }
    //Met les noms dans la liste
    *listeNom = malloc(sizeof(**listeNom)*nbMap);
    for(i = 0 ; i < nbMap ; i ++)
    {
        (*listeNom)[i] = malloc(sizeof(***listeNom)*MAX_NOM); // On suppose que le nom sera plus petit que MAX_NOM
    }
    rewinddir(directory);
    i = 0;
    while((fichierLu = readdir(directory)) != NULL)
    {
        if ((strcmp(fichierLu->d_name, ".") != 0) && (strcmp(fichierLu->d_name, "..") != 0))
        {
            strcpy((*listeNom)[i], dirName);
            strcat((*listeNom)[i], "/");
            strcat((*listeNom)[i], fichierLu->d_name);
            i++;
        }
    }
    return nbMap;
}
