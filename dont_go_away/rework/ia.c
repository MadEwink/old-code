#include <stdlib.h>
#include "main.h"
#include "ia.h"
#include "map.h"

int dir(int posG, int posF)
{
    int p;
    p = posG - posF;
    return 2*(p>0)-1;
}

Position deplG(int **carte, Position posG, Position posF)
{
    int dirX = dir(posG.x, posF.x);
    int dirY = dir(posG.y, posF.y);
    int possibleX;
    int possibleY;
    int procheX;
    int dirFinale = -1;

    possibleX = (carte[posG.x+dirX][posG.y] != MUR);
    possibleY = (carte[posG.x][posG.y+dirY] != MUR);
    if(possibleX == 0 && possibleY == 0)
    {
        dirX = -dirX;
        dirY = -dirY;
        possibleX = (carte[posG.x+dirX][posG.y] != MUR);
        possibleY = (carte[posG.x][posG.y+dirY] != MUR);
    }

    procheX = abs(posG.x-posF.x)-abs(posG.y-posF.y);

    if(procheX>0)
    {
        if(possibleY)
        {
            if(dirY == -1)
                dirFinale = HAUT;
            else
                dirFinale = BAS;
        }
        else if(possibleX)
        {
            if(dirX == -1)
                dirFinale = GAUCHE;
            else
                dirFinale = DROITE;
        }
    }
    else if(procheX<0)
    {
        if(possibleX)
        {
            if(dirX == -1)
                dirFinale = GAUCHE;
            else
                dirFinale = DROITE;
        }
        else if(possibleY)
        {
            if(dirY == -1)
                dirFinale = HAUT;
            else
                dirFinale = BAS;
        }
    }
    else
    {
        if(possibleX && possibleY)
        {
            possibleX = (carte[posG.x+2*dirX][posG.y] != MUR);
            if(possibleX)
            {
                if(dirX == -1)
                    dirFinale = GAUCHE;
                else
                    dirFinale = DROITE;
            }
            else if(possibleY)
            {
                if(dirY == -1)
                    dirFinale = HAUT;
                else
                    dirFinale = BAS;
            }
        }
        else if(possibleX)
        {
            if(dirX == -1)
                dirFinale = GAUCHE;
            else
                dirFinale = DROITE;
        }
        else if(possibleY)
        {
            if(dirY == -1)
                dirFinale = HAUT;
            else
                dirFinale = BAS;
        }
    }
    posG.orientation = dirFinale;
    posG.booleen = 1-posG.booleen;
    switch(dirFinale)
    {
        case HAUT:
            posG.y--;
            break;
        case BAS:
            posG.y++;
            break;
        case GAUCHE:
            posG.x--;
            break;
        case DROITE:
            posG.x++;
            break;
    }
    return posG;
}
