#ifndef DEF_MAP
#define DEF_MAP
enum {SOL, MUR, TROU, AVENTURE, JLIBRE, DEPAF, DEPAG};
void fillMap(int **carte, char* nomMap, Position *posG, Position *posF);
void delMap(int **carte);
#endif // DEF_MAP
