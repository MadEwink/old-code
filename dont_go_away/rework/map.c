#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "map.h"

void fillMap(int **carte, char *nomMap, Position *posF, Position *posG)
{
    int x, y;
    int caracInt;

    char carac;

    FILE *fichier = NULL;

    fichier = fopen(nomMap,"r");

    rewind(fichier);
    for(y = 0 ; y < HAUTEUR_MAP ; y++)
    {
        for(x = 0 ; x < LARGEUR_MAP ; x++)
        {
            carac = fgetc(fichier);
            caracInt = carac - '0';
            switch(caracInt)
            {
                case DEPAF:
                    (*posF).x = x;
                    (*posF).y = y;
                    carte[x][y] = SOL;
                    break;
                case DEPAG:
                    (*posG).x = x;
                    (*posG).y = y;
                    carte[x][y] = SOL;
                    break;
                default:
                    carte[x][y] = caracInt;
                    break;
            }
        }
        carac = fgetc(fichier);
        if(carac == '\r') {
            carac = fgetc(fichier);
        }
        if (carac != '\n') {
            fprintf(stderr, "Nombre de colonnes incorrect dans %s", nomMap);
            exit(-1);
        }
    }
    fclose(fichier);
}

void delMap(int **carte)
{
    int i;
    for(i = 0 ; i < HAUTEUR_MAP ; i++)
    {
        free(carte[i]);
    }
    free(carte);
}
