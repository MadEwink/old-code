#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#ifndef WIN32
    #include <sys/types.h>
#endif
#include <string.h>
#include <SDL/SDL.h>

#include "main.h"
#include "affichage.h"
#include "deplacement.h"
#include "ia.h"
#include "map.h"
#include "jeu.h"
#include "fichier.h"

int jeu(char *nomMap, SDL_Surface *ecran)
{
    int i;
    int continuer = 1;

    int **carte = NULL; // la carte � charger

    Position posF; //la position de la fille
    Position posG; //la position du gar�on

    Position posFinit; //en cas de reset
    Position posGinit;

    SDL_Event event;

    carte = malloc(sizeof(*carte)*LARGEUR_MAP);
    for(i = 0 ; i < LARGEUR_MAP ; i++)
    {
        carte[i] = malloc(sizeof(**carte)*HAUTEUR_MAP);
    }
    fillMap(carte, nomMap, &posF, &posG);

    posF.orientation = HAUT;
    posF.booleen = 0;

    posG.orientation = HAUT;
    posG.booleen = 0;

    posFinit = posF;
    posGinit = posG;

    while(continuer == 1)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = -1;
                break;
            case SDL_KEYUP:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        continuer = 0;
                        break;
                    case SDLK_r:
                        posF = posFinit;
                        posG = posGinit;
                        break;
                    default:
                        break;
                }
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_UP:
                        posF.orientation = HAUT;
                        posF = deplacement(posF, carte);
                        posG = deplG(carte, posG, posF);
                        posF.booleen = 1-posF.booleen;
                        break;
                    case SDLK_LEFT:
                        posF.orientation = GAUCHE;
                        posF = deplacement(posF, carte);
                        posG = deplG(carte, posG, posF);
                        posF.booleen = 1-posF.booleen;
                        break;
                    case SDLK_RIGHT:
                        posF.orientation = DROITE;
                        posF = deplacement(posF, carte);
                        posG = deplG(carte, posG, posF);
                        posF.booleen = 1-posF.booleen;
                        break;
                    case SDLK_DOWN:
                        posF.orientation = BAS;
                        posF = deplacement(posF, carte);
                        posG = deplG(carte, posG, posF);
                        posF.booleen = 1-posF.booleen;
                        break;
                    default:
                        break;
                }
            default:
                break;
        }
        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255,255,255));
        afficheMap(carte, ecran);
        affichePers(posF, posG, ecran);
        SDL_Flip(ecran);
        if(carte[posG.x][posG.y] == TROU)
            continuer = 2;
    }
    return continuer;
}

int jeuLibre(SDL_Surface *ecran) //menu pour choisir le niveau
{
    int nbMap = 0;
    int continuer = 1, retour = 1;
    int posCurseur = 0;
    int page = 0;
    int choixParPage = 8;
    int nbPages = 3;
    char currentLoc[300];
    char **listeNom = NULL;
    SDL_Event event;
    DIR* directory;

    // Initialisation de laliste des maps
    getcwd(currentLoc, 290);
    strcat(currentLoc, "/cartes/");
    directory = opendir(currentLoc);
    nbMap = listeMap(&listeNom, directory, currentLoc); //Met la liste dans listeNom, retourne le nombre de Maps
    while(continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
                retour = 0;
                break;
            case SDL_KEYUP:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        continuer=0;
                        retour = 1;
                        break;
                    default:
                        break;
                }
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_UP:
                        posCurseur --;
                        break;
                    case SDLK_DOWN:
                        posCurseur ++;
                        break;
                    case SDLK_LEFT:
                        page --;
                        break;
                    case SDLK_RIGHT:
                        page ++;
                        break;
                    case SDLK_RETURN:
                        int numCarte = page * 8 + posCurseur;
                        if (numCarte < nbMap ) {
                        retour = jeu(listeNom[numCarte], ecran);
                            if (retour == 2) {
                                continuer = 1;
                            } else {
                                continuer = 0;
                            }
                        }
                    default:
                        break;
                }
                if (page >= nbPages)
                {
                    page = 0;
                    posCurseur = 0;
                }
                else if (page < 0)
                {
                    page = nbPages - 1;
                    posCurseur = 0;
                }
                if (posCurseur >= choixParPage)
                {
                    posCurseur = 0;
                }
                else if (posCurseur < 0)
                {
                    posCurseur = choixParPage - 1;
                }
                break;
            default:
                break;
        }
    }

    closedir(directory);
    return retour;
}

int jeuAventure(SDL_Surface *ecran)
{
    int i;
    int continuer = 1;
    int nbMap = compteMap();

    char **nomMap;

    nomMap = malloc(sizeof(*nomMap)*nbMap);
    for(i = 0 ; i < nbMap ; i++)
    {
        nomMap[i] = malloc(sizeof(char)*100);
    }

    i = 0;
    setNomMap(nomMap[i], i);
    SDL_Event event;

    do
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = -1;
                break;
            case SDL_KEYUP:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        continuer = 0;
                    default:
                        break;
                }
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_RETURN:
                        continuer = jeu(nomMap[i], ecran);
                        if(continuer == 2)
                        {
                            if(i < compteMap()-1)
                            {
                                continuer = 1;
                                i++;
                                setNomMap(nomMap[i], i);
                            }
                            else
                            {
                                continuer = 0;
                            }
                        }
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
        SDL_Flip(ecran);
    }while(continuer == 1);
    for(i = 0 ; i < nbMap ; i++)
    {
        free(nomMap[i]);
    }
    free(nomMap);
    if(continuer == -1)
        return 0;
    else
        return 1;
}
