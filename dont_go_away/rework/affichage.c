#include <stdlib.h>
#include <SDL/SDL.h>
#include "main.h"
#include "invert.h"
#include "map.h"
#include "affichage.h"

void afficheMap(int **carte, SDL_Surface* ecran)
{
    int x, y;
    SDL_Surface *mur;
    SDL_Surface *trou;
    SDL_Surface *aventure;
    SDL_Surface *jLibre;
    SDL_Rect pos;
    mur = SDL_CreateRGBSurface(0, COTE_CASE, COTE_CASE, 32, 0, 0, 0, 0);
    trou = SDL_LoadBMP("sprites/trou.bmp");
    aventure = SDL_CreateRGBSurface(0, COTE_CASE, COTE_CASE, 32, 0, 0, 0, 0);
    SDL_FillRect(aventure, NULL, SDL_MapRGB(aventure->format, 255,0,0));
    jLibre = SDL_CreateRGBSurface(0, COTE_CASE, COTE_CASE, 32, 0, 0, 0, 0);
    SDL_FillRect(jLibre, NULL, SDL_MapRGB(jLibre->format, 0,255,0));
    for(x = 0 ; x < LARGEUR_MAP ; x++)
    {
        for(y = 0 ; y < HAUTEUR_MAP ; y++)
        {

            pos.x = COTE_CASE*x;
            pos.y = COTE_CASE*y;
            switch(carte[x][y])
            {
                case MUR:
                    SDL_BlitSurface(mur, NULL, ecran, &pos);
                    break;
                case TROU:
                    SDL_BlitSurface(trou, NULL, ecran, &pos);
                    break;
                case AVENTURE:
                    SDL_BlitSurface(aventure, NULL, ecran, &pos);
                    break;
                case JLIBRE:
                    SDL_BlitSurface(jLibre, NULL, ecran, &pos);
                default:
                    break;
            }
        }
    }
    SDL_FreeSurface(mur);
    SDL_FreeSurface(trou);
    SDL_FreeSurface(aventure);
}

void affichePers(Position posF, Position posG, SDL_Surface* ecran)
{
    SDL_Surface *surF = NULL;
    SDL_Surface *surG = NULL;

    SDL_Rect posAct;
    SDL_Rect dimension;

    switch(posF.orientation)
    {
        case DROITE:
        case GAUCHE:
            if(posF.booleen)
                surF = SDL_LoadBMP("sprites/FilleDroite.bmp");
            else
                surF = SDL_LoadBMP("sprites/FilleDroite2.bmp");
            break;
        case HAUT:
            surF = SDL_LoadBMP("sprites/FilleDos.bmp");
            break;
        case BAS:
            surF = SDL_LoadBMP("sprites/FilleBas.bmp");
            break;
        default:
            surF = SDL_LoadBMP("sprites/FilleDos.bmp");
    }
    if((posF.orientation == GAUCHE) || (((posF.orientation == HAUT) || (posF.orientation == BAS)) && posF.booleen))
        miroir(surF);
    SDL_SetColorKey(surF, SDL_SRCCOLORKEY, SDL_MapRGB(surF->format, 255, 255, 255));
    //Gar�on :
    switch(posG.orientation)
    {
        case DROITE:
        case GAUCHE:
            if(posG.booleen)
                surG = SDL_LoadBMP("sprites/HommeDroite.bmp");
            else
                surG = SDL_LoadBMP("sprites/HommeDroite2.bmp");
            break;
        case HAUT:
            surG = SDL_LoadBMP("sprites/HommeDos.bmp");
            break;
        case BAS:
            surG = SDL_LoadBMP("sprites/HommeBas.bmp");
            break;
        default:
            surG = SDL_LoadBMP("sprites/HommeDos.bmp");
    }
    if((posG.orientation == GAUCHE) || (((posG.orientation == HAUT) || (posG.orientation == BAS)) && posG.booleen))
        miroir(surG);
    SDL_SetColorKey(surG, SDL_SRCCOLORKEY, SDL_MapRGB(surG->format, 255, 255, 255));
    //Bas
    dimension.x = 0;
    dimension.y = COTE_CASE;
    dimension.w = COTE_CASE;
    dimension.h = COTE_CASE;
    //Gar�on :
    posAct.x = posG.x*COTE_CASE;
    posAct.y = (posG.y)*COTE_CASE;
    SDL_BlitSurface(surG, &dimension, ecran, &posAct);
    //Fille :
    posAct.x = posF.x*COTE_CASE;
    posAct.y = (posF.y)*COTE_CASE;
    SDL_BlitSurface(surF, &dimension, ecran, &posAct);
    //Haut
    dimension.x = 0;
    dimension.y = 0;
    dimension.w = COTE_CASE;
    dimension.h = COTE_CASE;
    //Gar�on :
    posAct.x = posG.x*COTE_CASE;
    posAct.y = (posG.y-1)*COTE_CASE;
    SDL_BlitSurface(surG, &dimension, ecran, &posAct);
    //Fille :
    posAct.x = posF.x*COTE_CASE;
    posAct.y = (posF.y-1)*COTE_CASE;
    SDL_BlitSurface(surF, &dimension, ecran, &posAct);

    SDL_FreeSurface(surF);
    SDL_FreeSurface(surG);
}
