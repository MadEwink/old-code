#include <stdlib.h>
#include "main.h"
#include "map.h"
#include "deplacement.h"

Position deplacement(Position posF, int **carte)
{
    switch(posF.orientation)
    {
        case HAUT:
            if(carte[posF.x][posF.y-1] != MUR && carte[posF.x][posF.y-1] != TROU)
                posF.y -= 1;
            break;
        case BAS:
            if(carte[posF.x][posF.y+1] != MUR && carte[posF.x][posF.y+1] != TROU)
                posF.y += 1;
            break;
        case GAUCHE:
            if(carte[posF.x-1][posF.y] != MUR && carte[posF.x-1][posF.y] != TROU)
                posF.x -= 1;
            break;
        case DROITE:
            if(carte[posF.x+1][posF.y] != MUR && carte[posF.x+1][posF.y] != TROU)
                posF.x += 1;
            break;
    }
    return posF;
}
