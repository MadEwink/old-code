#include <stdlib.h>
#include "main.h"
#include "map.h"
#include "deplacement.h"

Position deplacement(Position posF, int **carte)
{
    switch(posF.orientation)
    {
        case HAUT:
            if(carte[posF.x][posF.y-1] == SOL)
                posF.y -= 1;
            break;
        case BAS:
            if(carte[posF.x][posF.y+1] == SOL)
                posF.y += 1;
            break;
        case GAUCHE:
            if(carte[posF.x-1][posF.y] == SOL)
                posF.x -= 1;
            break;
        case DROITE:
            if(carte[posF.x+1][posF.y] == SOL)
                posF.x += 1;
            break;
    }
    return posF;
}
