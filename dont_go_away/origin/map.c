#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "map.h"

void fillMap(int **carte)
{
    int x, y;

    char carac;

    FILE *fichier = NULL;

    fichier = fopen("carte.map","r");

    rewind(fichier);
    for(y = 0 ; y < HAUTEUR_MAP ; y++)
    {
        for(x = 0 ; x < LARGEUR_MAP ; x++)
        {
            carac = fgetc(fichier);
            if(carac == '\n')
                carac = fgetc(fichier);
            if(carac == '0')
                carte[x][y] = SOL;
            else
                carte[x][y] = MUR;
        }
    }
    fclose(fichier);
}

void delMap(int **carte)
{
    int i;
    for(i = 0 ; i < HAUTEUR_MAP ; i++)
    {
        free(carte[i]);
    }
    free(carte);
}
