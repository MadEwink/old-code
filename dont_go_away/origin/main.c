#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include "main.h"
#include "invert.h"
#include "ia.h"
#include "map.h"
#include "affichage.h"
#include "deplacement.h"

int main(int argc, char* argv[])
{
    int continuer = 1;
    int i;

    int **carte = NULL;

    Position posF;
    Position posG;

    SDL_Surface *ecran;

    SDL_Event event;

    carte = malloc(sizeof(*carte)*LARGEUR_MAP);
    for(i = 0 ; i < LARGEUR_MAP ; i++)
    {
        carte[i] = malloc(sizeof(**carte)*HAUTEUR_MAP);
    }
    fillMap(carte);

    posF.x = 14;
    posF.y = 8;
    posF.orientation = HAUT;
    posF.booleen = 0;

    posG.x = 14;
    posG.y = 5;
    posG.orientation = HAUT;
    posG.booleen = 0;

    SDL_Init(SDL_INIT_VIDEO);

    SDL_WM_SetIcon(SDL_LoadBMP("sprites/icone.bmp"), NULL);
    SDL_WM_SetCaption("Don't go away", NULL);

    ecran = SDL_SetVideoMode(LARGEUR_ECRAN, HAUTEUR_ECRAN, 32, SDL_HWSURFACE|SDL_DOUBLEBUF);

    SDL_EnableKeyRepeat(50,50);

    while(continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
                break;
            case SDL_KEYUP:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        continuer = 0;
                        break;
                    default:
                        break;
                }
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_UP:
                        posF.orientation = HAUT;
                        posF = deplacement(posF, carte);
                        posG = deplG(carte, posG, posF);
                        posF.booleen = 1-posF.booleen;
                        break;
                    case SDLK_LEFT:
                        posF.orientation = GAUCHE;
                        posF = deplacement(posF, carte);
                        posG = deplG(carte, posG, posF);
                        posF.booleen = 1-posF.booleen;
                        break;
                    case SDLK_RIGHT:
                        posF.orientation = DROITE;
                        posF = deplacement(posF, carte);
                        posG = deplG(carte, posG, posF);
                        posF.booleen = 1-posF.booleen;
                        break;
                    case SDLK_DOWN:
                        posF.orientation = BAS;
                        posF = deplacement(posF, carte);
                        posG = deplG(carte, posG, posF);
                        posF.booleen = 1-posF.booleen;
                        break;
                    default:
                        break;
                }
            default:
                break;
        }
        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));
        afficheMap(carte, ecran);
        affichePers(posF, posG, ecran);
        SDL_Flip(ecran);
    }

    SDL_Quit();

    delMap(carte);

    return 0;
}
