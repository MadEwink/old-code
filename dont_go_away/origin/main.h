#ifndef DEF_MAIN
#define DEF_MAIN
#define COTE_CASE 16
#define HAUTEUR_MAP 20
#define LARGEUR_MAP 30
#define HAUTEUR_ECRAN HAUTEUR_MAP*COTE_CASE
#define LARGEUR_ECRAN LARGEUR_MAP*COTE_CASE
enum {HAUT, BAS, GAUCHE, DROITE};
typedef struct Position Position;
struct Position
{
    int x;
    int y;
    int orientation;
    int booleen;
};
#endif // DEF_MAIN
