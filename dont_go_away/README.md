# Don't go away

## Context

This game was developped for a creative contest organised by VoxMakers in 2015.

This contest gave a picture as a prompt, and people could create any art (image, texts, game, software) inspired by it.

At that time I was not using any kind of versioning tool, but I fortunately had broken some projects already, since it seems I copied the code before trying to improve it.

There are two folders in there.
- `origin` contains the code of the contest game
- `rework` contains the code after I tried to improve it

The `origin` code has remained untouched, because I understood in time that the only problem I had was a file format issue that I could fix without editing the code.

However, the `rework` that I found first was edited a bit to read the dos formatted file on linux. When digging it up, I was so sure I had broken it that I did not thought it could be that kind of error. Anyway, there were stuff that were actually broken, that I took the liberty to fix, which were mostly pointers errors.

If you are interrested in looking at something in here, I suggest you head towards the `origin` folder, which is not great, but I think the `rework` is just more of the same dirty stuff, on which was added dirtier stuff.

## The game

This is a simple turn by turn 2D puzzle game. You play against the computer.

You play as the red haired character, who runs after the blue shirt character.
In the `rework` version, your goal is to guide them towards the hole.

The computer uses a simple AI, which makes the blue shirt character move away from you each time you move.

## Dependencies

To compile you'll need SDL (1, not 2).
On linux, you can get the `libsdl1.2-dev` package.

## Compile

There is a makefile, so you should just be able to run `make` in either folder.

Otherwise, compile and assemble all the `.c` files, then link the resulting `.o` files. Don't forget to specify `-lSDL` when linking.

## Run

Just run the resulting exe `./dontgoaway` in the `bin` folder at your own risks.

In both versions you can move around using the arrow keys.
At any point, pressing escape gets you back / out.

### Origin version

This version just throws you into a map.

You can try to catch the blue shirt character, but I don't think it is possible to achieve.
Anyway, the game would not even detect you did it, so just mess up as you please in the map.

### Rework version

The title screen has the two characters, and two color zones. The one on the left launches "adventure mode" (complete a given list of maps one after another) and the one on the right launches "free mode" (choose any map you want to play from the available maps).

You can either go with your character on the zone and press return, or guide the other character on one of the zone.

Note that for "free mode", there is no way for you to know what you are selecting : you are navigating through pages of 8 maps, up and down lets you choose any map of the current page, and left and right let you change the current page, but you won't know what you're choosing until you hit return.
Also, you can obviously select empty spots, pressing return on them will have no effect.
