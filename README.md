# Old Projects by Mad

This repository will host some of my very old projects.
Those are small programs and games I created when I began programming.
The earliest code was written in 2009.

For each project, I will first commit the code as I found it. Some projects were modified between 2009 and something like 2014, some were not.
Some may not work at all due to modifications I tried to make back then to clean the code, add some features, etc.
Then I'll edit some stuff, at least I'll add a makefile, a readme. I don't think I was seaking english very well at the time, so the projects are mostly in french. I might translate them in english. I might also edit variable names to avoid Frenglish stuff.
Finally, I might edit the code to clean it. Most certainly will I fix errors, memory leaks, and stuff like that if I find some. I might also refactor anything I want, rewrite functions, change data structures, change save format, add features, etc.
