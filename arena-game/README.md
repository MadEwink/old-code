# Arena Game

This is kind of a rpg game console game.

You play a character that can go fight monsters in an arena to gain experience and gold.

The game is not really fun, you have no decision making and have to rely on the rng god smiling upon you, or on your skill in editing the text in the save file.

## Combat system

The combat is full rng.
The player and the mob take turns attacking each other.
When attacking, a fighter has a random chance of making a normal hit, a critical hit, a critical failure (self harm), or missing.

The probabilities for each is :
- normal attack : 2/6 for a mob, 3/6 for the player
- critical hit : 1/6
- critical failure : 1/6
- miss : 2/6 for a mob, 1/6 for the player

The mob inflicts 1HP on a normal hit and 3HP on a critical (including on itself on a critical failure).

The player has an attack stat which is added to a base value :
- normal attack : 1
- critical hit : 2
- critical failure : 3

The player can also choose to use an attack potion before the fight to up their attack points by one.

If the player dies during a fight, the game is over and the program exits.

## Enemies

There are several enemies the player can encounter.
They differ in their health and the amount of gold and xp they loot.

As a player levels up, they can access more mobs.

## Shop

The player can also visit a shop to buy health and attack potions.

## House

The player can visit their house to use a bought health potion.

## Save

The game can be saved upon quitting, and a save can be reloaded.
