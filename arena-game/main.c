#include <stdio.h>
#include <stdlib.h>
#include "combat.h"
#include "debut.h"
#include "magasin.h"
#include "maison.h"

int main()
{
    int *pointeurvie;
    int *pointeurniveau;
    int *pointeurexp;
    int *pointeurpointattaque;
    int *pointeurpointdefense;
    int *pointeurPO;
    int *pointeurpotionsoin;
    int *pointeurpotionattaque;
    int quefaire;
    int vie = 20;
    int niveau = 1;
    int exp = 0;
    int pointattaque = 1;
    int pointdefense = 1;
    int PO = 20;
    int potionsoin = 0;
    int potionattaque;
    int exp_aide = 0;
    int chargersauv;
    int sauvegarder;
    int score [7] = {0};
    FILE* fichier = NULL;
    fichier = fopen("sauvegarde.sauv","r+");

    printf("Bienvenue dans la deuxieme version de ce jeu.\n");

    printf("Voulez vous charger une sauvegarde?\n\n(1)Oui\n(2)Non\n");
    scanf("%d",&chargersauv);
    if (chargersauv == 1)
    {
        if (fichier != NULL)
        {
            fscanf(fichier, "%d %d %d %d %d %d %d", &score[0], &score[1], &score[2], &score[3] ,&score[4] ,&score[5] ,&score[6]);
            (pointeurvie = &score[0]);
            (pointeurpotionsoin = &score[1]);
            (pointeurexp = &score[2]);
            (pointeurPO = &score[3]);
            (pointeurpointattaque = &score[4]);
            (pointeurniveau = &score[5]);
            (pointeurpotionattaque = &score[6]);
            fclose(fichier);
        }
        else
        {
            (PO = 20);
            (exp = 0);
            (niveau = 1);
            (pointeurvie = &vie);
            (pointeurpotionsoin = &potionsoin);
            (pointeurexp = &exp);
            (pointeurPO = &PO);
            (pointeurpointattaque = &pointattaque);
            (pointeurniveau = &niveau);
            (pointeurpotionattaque = &potionattaque);
        }
    }
    else
    {
        (PO = 20);
        (exp = 0);
        (niveau = 1);
        (pointeurvie = &vie);
        (pointeurpotionsoin = &potionsoin);
        (pointeurexp = &exp);
        (pointeurPO = &PO);
        (pointeurpointattaque = &pointattaque);
        (pointeurniveau = &niveau);
        (pointeurpotionattaque = &potionattaque);
    }
    do
    {
        printf("Vous etes de niveau %d et vous avez %d points d'experience.\nVous avez %d vies. Vous avez %d points d'attaque et vous avez %d pieces d'or (PO)" , *pointeurniveau , *pointeurexp , *pointeurvie , *pointeurpointattaque , *pointeurPO);
        printf("\nVoulez vous aller:\n(1)A l'arene\n(2)Au magasin\n(3)Chez vous\n(4)Quitter.\n\n");
        scanf("%d",&quefaire);
        switch (quefaire)
        case 1:
        {
            combat(pointeurvie , pointeurniveau , pointeurexp , pointeurpointattaque , pointeurpointdefense , pointeurPO ,pointeurpointattaque);
            break;
        }
        switch (quefaire)
        case 2:
        {
            magasin (pointeurPO , pointeurpotionsoin , pointeurpotionattaque);
        break;
        }
        switch (quefaire)
        case 3:
        {
            maison(pointeurpotionsoin , pointeurvie);
            break;
        }
        switch (quefaire)
        case 4:
        {
            break;
        }
        if (*pointeurexp >= 2)
        {
            do
            {
                (*pointeurniveau += 1);
                (exp_aide = *pointeurexp - 2);
                (*pointeurexp = exp_aide);
            }while(*pointeurexp >= 2);
        }
        if (*pointeurniveau == 3)
        {
            (*pointeurpointattaque = 2);
        }
        else if (*pointeurniveau == 5)
        {
            (*pointeurpointattaque = 3);
        }
    }while (quefaire != 4);
    printf("Voulez vous sauvegarder?\n\n(1)Oui\n(2)Non\n");
    scanf("%d",&sauvegarder);
    if (sauvegarder == 1)
    {
        (score[0] = *pointeurvie);
        (score[1] = *pointeurpotionsoin);
        (score[2] = *pointeurexp);
        (score[3] = *pointeurPO);
        (score[4] = *pointeurpointattaque);
        (score[5] = *pointeurniveau);
        (score[6] = *pointeurpotionattaque);

        fichier = fopen("sauvegarde.sauv","w+");
        if (fichier != NULL)
        {
            fprintf(fichier,"%d %d %d %d %d %d", score[0] , score[1] , score[2] , score[3] , score[4] , score[5] , score[6]);
            fclose (fichier);
        }
        else
        {
            exit(0);
        }
    }
    else
    {

    }
    return 0;
}

