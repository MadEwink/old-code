#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "combat.h"

// il faut rajouter l'augmentat� du compteur a chaque lancer
void combat(int *pointeurvie , int *pointeurniveau ,int *pointeurexp , int *pointeurpointattaque ,int *pointeurpointdefense ,int *pointeurPO ,int *pointeurpotionattaque)
{
    int choix;
    int nombredelancers;
    int *pointeurviemob;
    int viedumob;
    (pointeurviemob = &viedumob);
    printf("Voulez vous utiliser une potion d'attaque...");
    scanf("%d",&choix);
    if (choix == 1 && pointeurpotionattaque > 0)
    {
        (pointeurpotionattaque --);
        (nombredelancers = 0);
        (pointeurpointattaque ++);
    }
    else
    {
        (nombredelancers = 0);
    }
    if (nombredelancers == 10)
    {
        (pointeurpointattaque --);
        (nombredelancers = 0);
    }
    else;
    viemob (pointeurviemob, pointeurniveau, pointeurvie, pointeurexp, pointeurPO ,pointeurpointattaque);
}


void viemob (int *pointeurviemob,int *pointeurniveau,int *pointeurvie ,int *pointeurexp ,int *pointeurPO ,int *pointeurpointattaque)
{
    int continuer;
    int MAX;
    int MIN = 0;
    int choixmob;
    if (*pointeurniveau == 1)
    {
        printf("Vous etes de niveau 1.\nVous devez donc vous battre contre un blob\nEntrez 1 pour continuer.\n");
        scanf("%d",&continuer);
        (*pointeurviemob = 1);
        fight(pointeurviemob , pointeurvie , pointeurexp , pointeurniveau ,pointeurpointattaque);
        (*pointeurexp += 1);
        (*pointeurPO += 5);
    }
    else if (*pointeurniveau > 1 && *pointeurniveau < 3)
    {
        printf("Vous avez depasse le niveau 1 mais vous n'etes pas encore tres fort.\nVous avez donc encore un choix limite.\nEntrez 1 pour continuer\n");
        scanf("%d",&continuer);
        (MIN = 1);
        (MAX = 3);
        srand(time(NULL));
        choixmob = (rand() % (MAX - MIN + 1)) + MIN;
        quelmob (pointeurviemob ,choixmob ,pointeurvie ,pointeurexp ,pointeurniveau ,pointeurPO ,pointeurpointattaque);
    }
    else
    {
        if (*pointeurniveau == 3 && *pointeurexp == 0)
        {
            printf("Vous vous accrochez. Vous avez maintenant la totalite des monstres a votre disposition.\nEntrez 1 pour continuer\n");
            scanf("%d",&continuer);
        }
        (MIN = 1);
        (MAX = 6);
        srand(time(NULL));
        choixmob = (rand() % (MAX - MIN + 1)) + MIN;
        quelmob (pointeurviemob ,choixmob ,pointeurvie ,pointeurexp ,pointeurniveau ,pointeurPO ,pointeurpointattaque);
    }
}



void quelmob (int *pointeurviemob ,int choixmob ,int *pointeurvie ,int *pointeurexp ,int *pointeurniveau ,int *pointeurPO ,int *pointeurpointattaque)
{
    printf ("Le tirage au sort a decide que vous affronteriez");
    if (choixmob == 1)
    {
        printf(" un blob");
        (*pointeurviemob = 1);
        fight(pointeurviemob , pointeurvie , pointeurexp , pointeurniveau ,pointeurpointattaque);
        (*pointeurexp += 1);
        (*pointeurPO += 5);
        printf("Vous gagnez 1 point d'experience et 5 PO.\n");
    }
    else if (choixmob == 2)
    {
        printf(" un moblin");
        (*pointeurviemob = 3);
        fight(pointeurviemob , pointeurvie , pointeurexp , pointeurniveau ,pointeurpointattaque);
        (*pointeurexp += 1);
        (*pointeurPO += 5);
        printf("Vous gagnez 1 point d'experience et 5 PO.\n");
    }
    else if (choixmob == 3)
    {
        printf(" un oktorok");
        (*pointeurviemob = 4);
        fight(pointeurviemob , pointeurvie , pointeurexp , pointeurniveau ,pointeurpointattaque);
        (*pointeurexp += 2);
        (*pointeurPO += 10);
        printf("Vous gagnez 2 points d'experience et 10 PO.\n");
    }
    else if (choixmob == 4)
    {
        printf(" un goblin");
        (*pointeurviemob = 5);
        fight(pointeurviemob , pointeurvie , pointeurexp , pointeurniveau ,pointeurpointattaque);
        (*pointeurexp += 2);
        (*pointeurPO += 15);
        printf("Vous gagnez 2 points d'experience et 15 PO.\n");
    }
    else if (choixmob == 5)
    {
        printf(" une plante carnivore");
        (*pointeurviemob = 7);
        fight(pointeurviemob , pointeurvie , pointeurexp , pointeurniveau ,pointeurpointattaque);
        (*pointeurexp += 5);
        printf("Vous gagnez 5 points d'experience.\n");
    }
    else if (choixmob == 6)
    {
        printf(" un chien des enfer");
        (*pointeurviemob = 8);
        fight(pointeurviemob , pointeurvie , pointeurexp , pointeurniveau ,pointeurpointattaque);
        (*pointeurexp += 5);
        (*pointeurPO += 5);
        printf("Vous gagnez 5 points d'experience et 5 PO.\n");
    }
}



void attaquepers (int *pointeurviemob ,int *pointeurvie ,int *pointeurpointattaque)
{
    int degats;
    int lancer;
    int MAX = 6;
    int MIN = 0;
    int de;
    printf("\nLance un de pour attaquer:\n(Appuie sur (1) pour lancer)\n");
    scanf("%d",&lancer);
    srand(time(NULL));
    de = (rand() % (MAX - MIN + 1)) + MIN;
    printf("Resultat du lancer: %d \n", de);
    if (de >= 3 && de !=6)
    {
        (*pointeurviemob -= 1 + *pointeurpointattaque);
        (degats = 1 + *pointeurpointattaque);
        printf("Attaque reussie, le monstre pert %d point de vie.\n" ,degats);
    }
    else if (de == 6)
    {
        (*pointeurviemob -= 2 + *pointeurpointattaque);
        (degats = 2 + *pointeurpointattaque);
        printf("Attaque critique! Le monstre pert %d points de vie.\n" ,degats);
    }
    else if (de == 0)
    {
        (*pointeurvie -= 3 + *pointeurpointattaque);
        (degats = 3 + *pointeurpointattaque);
        printf("Echec critique! Vous perdez %d points de vie.\n" ,degats);
    }
    else
    {
        printf("Attaque manquee, le monstre ne pert pas de vie.\n");
    }
}

void attaquemob (int *pointeurvie ,int *pointeurviemob)
{
    int MAX = 6;
    int MIN = 0;
    int de;
    printf("Au monstre d'attaquer:\n");
    srand(time(NULL));
    de = (rand() % (MAX - MIN + 1)) + MIN;
    printf("Resultat du lancer: %d \n",de);
    if (de > 3 && de != 6)
    {
        printf("Attaque reussie, vous perdez 1 point de vie.\n");
        (*pointeurvie -= 1);
    }
    else if (de == 6)
    {
        printf("Attaque critique! Vous perdez 3 points de vie.\n");
        (*pointeurvie -= 3);
    }
    else if (de == 0)
    {
        printf("Echec critique! Le monstre pert 3 points de vie.\n");
        (*pointeurviemob -= 3);
    }
    else
    {
        printf("Attaque manquee, vous ne perdez pas de vie.\n");
    }
}

void fight(int *pointeurviemob ,int *pointeurvie ,int *pointeurexp ,int *pointeurniveau ,int *pointeurpointattaque)
{
    int continuer;
    do
    {
        attaquepers (pointeurviemob ,pointeurvie ,pointeurpointattaque);
        if (*pointeurviemob < 0)
        {
            (*pointeurviemob = 0);
        }
        if (*pointeurvie <= 0)
        {
            (*pointeurvie = 0);
        }
        printf("Il reste %d vies au monstre et il vous reste %d vies.\nAppuyez sur 1 pour continuer.\n",*pointeurviemob ,*pointeurvie);
        scanf("%d",&continuer);
        if (*pointeurviemob >= 1)
        {
            attaquemob (pointeurvie ,pointeurviemob);
            if (*pointeurvie <= 0)
            {
                (*pointeurvie = 0);
            }
            if (*pointeurviemob < 0)
            {
                (*pointeurviemob = 0);
            }
            printf("Il vous reste %d vies et il reste %d vies au monstre.\nAppuyez sur 1 pour continuer.\n",*pointeurvie ,*pointeurviemob);
            scanf("%d",&continuer);
        }
        if (*pointeurvie <= 0)
        {
            printf("Vous etes mort\n");
            exit(0);
        }
        else{ }
    }while (*pointeurviemob >= 1 && *pointeurvie >=1);
}

