#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void crypteCesar(char* retour, char* txClair, char decalage)
{
    int i = 0;
    char mod = 'z'-'a' + 1;
    while (decalage < 0) {
        decalage += mod;
    }
    while (txClair[i] != '\0')
    {
        retour[i] = txClair[i];
        if (txClair[i] >= 'a' && txClair[i] <= 'z')
        {
            char c = (txClair[i]-'a'+decalage);
            retour[i] = c%mod +'a';
        }
        i++;
    }
    retour[i] = '\0';
}

void replaceEOL(char* text, int size) {
    char* c = strchr(text, '\n');
    if (c == NULL) {
        c = text+(size-1);
    }
    *c = '\0';
}

int main()
{
    char texteClair[100] = "\0";
    char texteCode[100] = "\0";
    char texteDeCode[100] = "\0";

    printf("Enter your text here : ");
    fgets(texteClair, 99, stdin);
    replaceEOL(texteClair, 100);

    char offset = 'd';
    printf("Enter the letter you want to correspond to a : ");
    offset = fgetc(stdin) - 'a';

    crypteCesar(texteCode, texteClair, offset);
    crypteCesar(texteDeCode, texteCode, -offset);
    printf("Encoded text : %s\n", texteCode);
    printf("Decoded text : %s\n", texteDeCode);
    return 0;
}
