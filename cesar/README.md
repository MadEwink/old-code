# Cesar

This is a simple program, that performs a caesar cipher on a string (only on lower case letters)

## Original version

According to the modification dates of the source code, I coded that in 2017.

The original code (which is available on the tag `cesar-v1`:ce2f4ad4) has a hardcoded input string and offset.

It also does not work on decoding when values go negative.

## Changes made

I took the liberty of adding some stuff :
- the string is now an input
- the offset is also an input
- the whole thing works for all lower case letters and offsets
